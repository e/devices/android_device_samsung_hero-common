# Copyright (C) 2012 The Android Open Source Project
# Copyright (C) 2013-2016 The CyanogenMod Project
# Copyright (C) 2018 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import common
import os
def FullOTA_PostValidate(info):
	info.script.AppendExtra('run_program("/tmp/install/bin/e2fsck_static", "-fy", "/dev/block/platform/155a0000.ufs/by-name/SYSTEM");');
	info.script.AppendExtra('run_program("/tmp/install/bin/resize2fs_static", "/dev/block/platform/155a0000.ufs/by-name/SYSTEM");');
	info.script.AppendExtra('run_program("/tmp/install/bin/e2fsck_static", "-fy", "/dev/block/platform/155a0000.ufs/by-name/SYSTEM");');


def FullOTA_InstallEnd(info):
	radio_path = os.path.join(common.OPTIONS.input_tmp, "RADIO","radio.img")
	if os.path.exists(radio_path) :
		info.script.Print("Writing radio...")
		common.ZipWrite(info.output_zip, radio_path, "radio.img")
		info.script.WriteRawImage("/radio", "radio.img")

	info.script.AppendExtra('mount("ext4", "EMMC", "/dev/block/platform/155a0000.ufs/by-name/SYSTEM", "/system", "");')
	info.script.AppendExtra('mount("ext4", "EMMC", "/dev/block/platform/155a0000.ufs/by-name/DATA", "/data", "");')
	info.script.AppendExtra('run_program("/sbin/sed","-i", "/p2p_disabled=1/d", "/data/misc/wifi/wpa_supplicant.conf");')

